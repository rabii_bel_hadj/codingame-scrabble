package com.scrabble;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class Test {
	static boolean isContainsWord(String mot, String lettresRestantes) {
		int count = 0;
		if (lettresRestantes.length() < mot.length()) {
			return false;
		}
		for (int i = 0; i < mot.length(); i++) {
			for (int j = 0; j < lettresRestantes.length(); j++) {
				if (mot.charAt(i) == lettresRestantes.charAt(j)) {
					count++;
					StringBuilder sb = new StringBuilder(lettresRestantes);
					sb.deleteCharAt(j);
					lettresRestantes = new String(sb);
					break;
				}
			}
		}
		if (count == mot.length()) {
			return true;
		}
		return false;
	}

	static int calculatePuissance(char c) {
		int puissance = 0;
		String onePoint = "eaionrtlsu";
		String twoPoints = "dg";
		String threePoints = "bcmp";
		String fourPoints = "fhvwy";
		String fivePoints = "k";
		String eightPoints = "jx";
		String tenPoints = "qz";

		String val = Character.toString(c);

		if (onePoint.contains(val)) {
			puissance = puissance + 1;
		} else if (twoPoints.contains(val)) {
			puissance = puissance + 2;
		} else if (threePoints.contains(val)) {
			puissance = puissance + 3;
		} else if (fourPoints.contains(val)) {
			puissance = puissance + 4;
		} else if (fivePoints.contains(val)) {
			puissance = puissance + 5;
		} else if (eightPoints.contains(val)) {
			puissance = puissance + 8;
		} else if (tenPoints.contains(val)) {
			puissance = puissance + 10;
		}
		return puissance;
	}

	static String getWordWithMaxPoints(List<String> dictionnaire, String lettresRestantes) {
		int puissance;
		TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
		for (String mot : dictionnaire) {
			puissance = 0;
			if (lettresRestantes.equals(mot)) {
				for (int i = 0; i < lettresRestantes.length(); i++) {
					puissance = puissance + calculatePuissance(lettresRestantes.charAt(i));
				}
				treeMap.put(new Integer(puissance), lettresRestantes);
			} else {
				if (isContainsWord(mot, lettresRestantes)) {
					for (int i = 0; i < mot.length(); i++) {
						puissance = puissance + calculatePuissance(mot.charAt(i));
					}
					if (!treeMap.containsKey(puissance))
						treeMap.put(new Integer(puissance), mot);
				}
			}
		}
		return treeMap.lastEntry().getValue().toString();
	}

	public static void main(String[] args) {
		List<String> mots = new ArrayList<String>();

		mots.add("because");
		mots.add("first");
		mots.add("these");
		mots.add("could");
		mots.add("which");

		String lettresRestantes = "hicquwh";

		System.out.println(getWordWithMaxPoints(mots, lettresRestantes));
	}
}
